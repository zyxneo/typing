import { Link } from 'gatsby';
import { FormattedMessage } from 'gatsby-plugin-intl';
import Cookies from 'js-cookie';
import React from 'react';

import { Button } from '@components';
import { ROUTE_PATH_PRIVACY_POLICY } from '@routes';
import { mountWithIntl } from '@utils/intlEnzymeTestHelper';

export const ANALYTICS_CONSENT_COOKIE_NAME = 'gatsby-gdpr-google-analytics';

import CookieConsent from './CookieConsent';

describe('<CookieConsent>', () => {
  it('renders properly', () => {
    const wrapper = mountWithIntl(<CookieConsent />);

    expect(wrapper.find('.cookieConsent')).toHaveLength(1);
    expect(
      wrapper
        .find('.cookieConsent__desc')
        .find(FormattedMessage)
        .first()
        .prop('id')
    ).toEqual('site.cookieConsent.desc');

    const privacyLink = wrapper.find(Link);
    expect(privacyLink).toHaveLength(1);
    expect(privacyLink.prop('to')).toBe(ROUTE_PATH_PRIVACY_POLICY);
    expect(privacyLink.find(FormattedMessage).first().prop('id')).toEqual(
      'site.cookieConsent.moreDetails'
    );

    const okButton = wrapper.find(Button);
    expect(okButton).toHaveLength(1);
    expect(okButton.prop('className')).toBe('cookieConsent__ok');
    expect(okButton.find(FormattedMessage).first().prop('id')).toEqual(
      'site.cookieConsent.allow'
    );
    const cookieExist = Cookies.get(ANALYTICS_CONSENT_COOKIE_NAME);
    expect(cookieExist).toBe(undefined);
  });

  it('sets cookie consent on clickink OK button', () => {
    const wrapper = mountWithIntl(<CookieConsent />);

    const okButton = wrapper.find(Button);
    okButton.simulate('click');
    const cookieExist = Cookies.get(ANALYTICS_CONSENT_COOKIE_NAME);
    expect(cookieExist).toBe('true');
  });

  it('does not appear if cookie is already set', () => {
    const wrapper = mountWithIntl(<CookieConsent />);
    expect(wrapper.find('.cookieConsent')).toHaveLength(0);
  });
});
