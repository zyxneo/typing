import { useState, useEffect } from 'react';

import { UserProfileProps } from '@components';
import useFirebase from '@utils/useFirebase';

export default function onlineUserCount(): number {
  const [user, setUser] = useState({} as UserProfileProps);
  const [onlineUsers, setOnlineUsers] = useState(0);

  const firebase = useFirebase();

  useEffect(() => {
    if (!firebase) return;
    // @ts-ignore
    return firebase.auth().onAuthStateChanged((user: any) => {
      setUser(user as UserProfileProps);
    });
  }, [firebase]);

  useEffect(() => {
    // https://firebase.googleblog.com/2013/06/how-to-build-presence-system.html
    // @ts-ignore
    if (!firebase) return;
    // @ts-ignore
    const usersOnlineRef = firebase.database().ref('usersOnline/');

    // If the user is logged in, add to the count
    if (user && user.uid) {
      // @ts-ignore
      const userIsOnlineRef = firebase.database().ref('.info/connected');
      // @ts-ignore
      const userRef = firebase.database().ref('usersOnline/' + user.uid);
      // @ts-ignore
      userIsOnlineRef.on('value', function (snapshot) {
        if (snapshot.val()) {
          userRef.onDisconnect().remove();
          userRef.set(user.displayName);
        }
      });
    }

    // Count all online users
    // @ts-ignore
    usersOnlineRef.on('value', function (snap) {
      setOnlineUsers(snap.numChildren());
    });
  }, [user]);

  return onlineUsers;
}
