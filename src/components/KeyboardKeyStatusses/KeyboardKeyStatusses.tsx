import { useIntl, FormattedMessage } from 'gatsby-plugin-intl';
import React from 'react';

import { KeyboardKey } from '@components';
import variables from '@theme/variables';

import './KeyboardKeyStatusses.scss';

type StateItemProps = {
  desc: string;
  key: {};
  title: string;
};

const KeyboardKeyStatusses = () => {
  const intl = useIntl();
  const { keyHeight, keyPaddingX, keyPaddingY, keyWidth } = variables;

  let calculatedX = 0;
  let calculatedY = 0;
  let calculatedWidth = keyWidth - keyPaddingX * 2;
  let calculatedHeight = keyHeight - keyPaddingY * 2;

  function renderKey(props = {}) {
    return (
      <svg
        className="statuses__keysvg"
        version="1.1"
        viewBox={`0 0 100 100`}
        textAnchor="middle"
      >
        <KeyboardKey
          x={calculatedX}
          y={calculatedY}
          width={calculatedWidth}
          height={calculatedHeight}
          displayedLevel="to"
          layout="102/105-ISO"
          iso="C01"
          code="KeyA"
          hand="left"
          finger="little"
          {...props}
        />
      </svg>
    );
  }

  function renderStatusItem({ desc, key, title }: StateItemProps) {
    return (
      <section className="statuses__item" key={title}>
        <div className="statuses__key">{renderKey(key)}</div>
        <div className="statuses__text">
          <h3 className="statuses__title">{title}</h3>
          <div className="statuses__desc">{desc}</div>
        </div>
      </section>
    );
  }

  const states: StateItemProps[] = [
    {
      key: {},
      title: intl.formatMessage({
        id: 'typing.key.unexplored',
        defaultMessage: 'Unexplored',
      }),
      desc: intl.formatMessage({
        id: 'state.unexplored.desc',
        defaultMessage:
          'This key is yet unknown. Be patient, you will discover it soon.',
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            toLearn: true,
          },
        },
      },
      title: intl.formatMessage({
        id: 'key.lesson.unexplored',
        defaultMessage: "Unexplored lesson's key",
      }),
      desc: intl.formatMessage({
        id: 'key.lesson.unexplored.desc',
        defaultMessage:
          'This key types the character which is in the focus of the current lesson. But the character is not yet known, so you have discover the character by pressing the key. You will be asked if you have to do so.',
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            label: 'a',
          },
        },
      },
      title: intl.formatMessage({
        id: 'key.default',
        defaultMessage: 'Default',
      }),
      desc: intl.formatMessage({
        id: 'key.default.desc',
        defaultMessage:
          'This key has no special property, this is how a regular key looks like.',
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            label: 'Æ',
          },
        },
      },
      title: intl.formatMessage({
        id: 'key.keytops',
        defaultMessage: 'Labels',
      }),
      desc: intl.formatMessage({
        id: 'key.keytops.desc',
        defaultMessage:
          'This key is the same, but it displays a label on a different level. These characters also called keytop. By pressing and holding the modifier keys like Shift the corresponding label becomes visible. You also can hover with your mouse on the keyboard to see the list discovered labels on each levels',
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            toLearn: true,
            label: 'a',
          },
        },
      },
      title: intl.formatMessage({
        id: 'key.lesson',
        defaultMessage: "Lesson's key",
      }),
      desc: intl.formatMessage({
        id: 'key.lesson.desc',
        defaultMessage:
          'This key types the character which is in the focus of the current lesson. That is why you have to pay more attention on it.',
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            learned: true,
            label: 'a',
          },
        },
      },
      title: intl.formatMessage({
        id: 'key.learned',
        defaultMessage: 'Learned',
      }),
      desc: intl.formatMessage({
        id: 'key.learned.desc',
        defaultMessage:
          'The character on this key is learned, because you succeed the lesson for this character, so the key is marked.',
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            label: 'a',
          },
        },
        marker: 'toPressFirst',
      },
      title: intl.formatMessage({
        id: 'key.pressfirst',
        defaultMessage: 'To press (first)',
      }),
      desc: intl.formatMessage({
        id: 'key.pressfirst.desc',
        defaultMessage:
          'If you see a key like this, hit it immediately. The fingers marked like this means that they has to be used to press the key. The characters on the board are in focus. So use this finger to press this key to write this character.',
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            label: 'a',
          },
        },
        marker: 'toPressSecond',
      },
      title: intl.formatMessage({
        id: 'key.pressnext',
        defaultMessage: 'To press (next)',
      }),
      desc: intl.formatMessage({
        id: 'key.pressnext.desc',
        defaultMessage:
          'Sometimes, in order to write a character you need to press not only one key, but two after each other. This vizualizations warns you, that after pressing a key, you will not see the output, but you have to press the one like this too.',
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            label: 'a',
          },
        },
        succeedState: 'correct',
      },
      title: intl.formatMessage({
        id: 'key.correct',
        defaultMessage: 'Correct',
      }),
      desc: intl.formatMessage({
        id: 'key.correct.desc',
        defaultMessage:
          'Well done, if you see this. It means, that you pressed the correct key. I can not tell, if you used the right finger, but I assume, so I mark the corresponding finger also correct.',
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            label: 'a',
          },
        },
        succeedState: 'missed',
      },
      title: intl.formatMessage({
        id: 'key.missed',
        defaultMessage: 'Missed',
      }),
      desc: intl.formatMessage({
        id: 'key.missed.desc',
        defaultMessage:
          'You make a mistake. You had to press this button, but you did not. You had to use this finger, but you did not. You should write this character, but again, you made a mistake instead.',
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            label: 'a',
          },
        },
        succeedState: 'error',
      },
      title: intl.formatMessage({
        id: 'key.error',
        defaultMessage: 'Error',
      }),
      desc: intl.formatMessage({
        id: 'key.error.desc',
        defaultMessage:
          "If you make a mistake, it means you did not done the right thing, but, well you did this instead. This was the error, you pressed this key, but you shouldn't. You probably used even a wrong finger, if you at least placed your finger correctly on the key - but that I can not tell for sure.",
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            label: 'a',
          },
        },
        pressure: 'pressed',
      },
      title: intl.formatMessage({
        id: 'key.pressed',
        defaultMessage: 'Pressed',
      }),
      desc: intl.formatMessage({
        id: 'key.pressed.desc',
        defaultMessage:
          "You are pressing this key right now. (If not, it is stucked, but don't worry, it will be released if you press something else afterward.)",
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            label: 'a',
          },
        },
        pressure: 'locked',
      },
      title: intl.formatMessage({
        id: 'key.locked',
        defaultMessage: 'Locked',
      }),
      desc: intl.formatMessage({
        id: 'key.locked.desc',
        defaultMessage:
          'There are some specific toggle keys on the keyboard, like e.g. Caps Lock. It does not make sense to learn while they are switched on, so if you see this, turn it off please.',
      }),
    },
    {
      key: {
        keyTops: {
          to: {
            label: '^',
            dead: true,
          },
        },
      },
      title: intl.formatMessage({
        id: 'key.dead',
        defaultMessage: 'Dead',
      }),
      desc: intl.formatMessage({
        id: 'key.dead.desc',
        defaultMessage:
          "It's a dead key. There's nothing wrong with it. Actually it is a magical key. By pressing once, it does noting, just prepares a diacritic for character composion. Pressing twice it writes the diacritic glyph, this way it can be discovered and identified. But the real point is that pressing this key, and pressing a regular letter - may - combines them to a third character. Let me tell you an example. If the dead key is ^, you can combine it with the character a and so write â. This way you can write an enormous amount of wird, totally unusable characters, like á, à, ä, å, ạ, ȧ and so on. You will like it!",
      }),
    },
  ];

  return (
    <section className="statuses">
      <h3>
        <FormattedMessage id="statuses.title" defaultMessage="Statusses" />
      </h3>

      <p>
        <FormattedMessage
          id="statuses.desc"
          defaultMessage="The following color definitions are valid for keys, hand fingers and the lesson text too."
        />
      </p>

      <div className="statuses__list">
        {states.map((item) => renderStatusItem(item))}
      </div>
    </section>
  );
};

export default KeyboardKeyStatusses;
