import { useIntl, Link, FormattedMessage } from 'gatsby-plugin-intl';
import React from 'react';

import { Layout, SEO } from '@components';
import { ROUTE_PATH_CONTACT } from '@routes';

export default function ContributionPage() {
  const intl = useIntl();

  function HungarianContribution() {
    return (
      <>
        <h3>A programról</h3>
        <p>
          A program elődjét eredetileg saját részre készítettem, gondoltam,
          legyen valami segítségemre a gépírás tanulásában. Azután úgy alakult,
          hogy egyre többet foglalkoztam a program írogatásával, mint a gépírás
          tanulásával. 2008 óta fejlesztem, ha időm engedi. Az időm viszont
          sajnos túl kevés, hogy mindent egyszerre csináljak, ezért kérem, hogy
          akinek ideje engedi és kedve tartja az segítsen ebben. A program nyílt
          forráskódú, így bárki szabadon beletekinthet, másolhatja, vagyis
          szabadon elérhető és terjeszthető.
        </p>

        <h3>Jelezd a hibákat!</h3>
        <p>
          Valami nem müködik? Nem úgy működik ahogyan szeretnéd? Kérlek
          okvetlenül <Link to={ROUTE_PATH_CONTACT}>jelezd</Link>, hogy ki tudjam
          javítani!
        </p>

        <h3>Van egy jó ötleteted?</h3>
        <p>
          Kérlek okvetlenül <Link to={ROUTE_PATH_CONTACT}>jelezd</Link>, hogy
          idővel be tudjam építeni.
        </p>

        <h3>Támogasd anyagilag az ingyenes gépírás oktatót!</h3>
        <p>
          De miért kellene fizetni, ha egyszer ingyenes?! Noha a program
          ingyenesen elérhető és használható az egész interneten, ennek az
          egyetlen oka az, hogy én ingyen megcsináltam, rendelkezésedre
          bocsájtottam és kifizetem a számlákat ami a domain, a tárhely, meg az
          internet hozzáféréshez kell. Ezek eleinte nem nagy összegek voltak, de
          ahhoz vezetnek, hogy míg neked ingyenes, nekem mégiscsak fizetős{' '}
          <span role="img" aria-label="face with head-bandage">
            🤕
          </span>
          . Jelenleg már nem magamnak csinálom, hanem neked, vagy a
          gyermekeidnek. Az én hasznom ebből az, hogy tanulom a szakmám,
          miközben barátokat és haragosokat gyűjtök.
        </p>
        <p>
          Ha tetszik a program, és úgy látod, megéri támogatni, bármikor
          szabadon megteheted. Anyagi hozzájárulásod biztosítja, hogy továbbra
          is elérhető legyen és fejlődjön.{' '}
          <strong>Minden egyes forint egy szavazat és bátorítás!</strong> Hogy
          konkrétan milyen összeget szánnál rá, azt rajtad áll, de ha
          bizonytalan lennél, íme pár javaslat:
        </p>
        <ul>
          <li>
            <h3>
              <span role="img" aria-label="banana, monkey">
                🍌 🐒
              </span>{' '}
              Banán - 500 Ft
            </h3>
            <p>
              A "
              <a
                href="http://www.urbandictionary.com/define.php?term=code%20monkey"
                target="_blank"
              >
                code monkey
              </a>
              "-t néha meg kell etetni, és ennyi pénzért el van látva egy napra
              gyümölccsel. Igazán nem sok, rosszabb helyre is elmehetne ennyi
              pénz{' '}
              <span role="img" aria-label="winking face">
                😉
              </span>{' '}
              Ha nincsenek anyagi gondjaid, megfizethető.
            </p>
          </li>
          <li>
            <h3>
              <span role="img" aria-label="cigar, bier">
                🚬 🍺
              </span>{' '}
              Cigi vagy sör - 1000 Ft
            </h3>
            <p>
              A munka melletti programozás csak növeli az idegi megterhelést.
              Bár nem dohányzom, nem iszom, és{' '}
              <a
                href="https://www.youtube.com/watch?v=piFTD7jVrYE"
                target="_blank"
              >
                másként kezelem a stresszt
              </a>
              , ez az összeg egy nagylelkű ajándék! (És a munkaerőpiaci árakat
              tekintve egy jól fizetett programozó akár 15 percet is hajlandó
              dolgozni érte. 😎) Ha magad is dolgozol, és az itt szerzett
              képességek segítettek benne, ezzel megfizeted a munkám és mindenki
              boldog!
            </p>
          </li>
          <li>
            <h3>
              <span role="img" aria-label="bouquet, champagne">
                💐 🍾
              </span>{' '}
              Virágcsokor vagy pezsgő - 2500 Ft (új!)
            </h3>
            <p>
              Kijavítottam a hibát ami idegesített, vagy implementáltam végre
              valami jó kis funkciót amit nagyon vártál már? Akkor ideje
              ünnepelni.
            </p>
          </li>
          <li>
            <h3>
              <span role="img" aria-label="book">
                📕
              </span>{' '}
              Szakkönyv - 5000 Ft
            </h3>
            <p>
              A technika folyton változik. Lehet, hogy valaki lépést tud vele
              tartani, de nekem eddig még nem sikerült.{' '}
              <span role="img" aria-label=" 	grinning face with sweat">
                😅
              </span>{' '}
              Ez az összeg a továbbképzés kategóriája, igazi mecénásoknak,
              művészetpártolóknak írtam ki. Ha tanfolyamokhoz használod a
              programot, vagy vállalkozásod megerősítésére, akkor egy nagyobb
              összeggel járulhatsz hozzá, hogy a program fejlődjön, és mind
              jobban kiszolgálja a te személyes igényeidet is.
            </p>
          </li>
          <li>
            <h3>
              <span role="img" aria-label="luxury yacht">
                🛳
              </span>{' '}
              Luxusjacht - 6000000000 Ft (új!)
            </h3>
            <p>
              Jelenleg a hálószobában dolgozok egy helyes kis asztalkán, de
              gondoltam jobb lenne valami egzokikus sziget felé menet egy
              luxusjacht fedélzetén nyomogatni a billentyüzetet... Biztosan
              behánynék a tengeri hullámokon, de megérné. Meg az egzotikus
              szigeteken egzotikus betegségek is vannak, ezért be is kellene
              oltatni magam... Most így jobban belegondolva... egy banán ára
              szinte semmi, nem? És az is egzotikus.
            </p>
          </li>
        </ul>

        <h4>Adatok Forint átutaláshoz</h4>
        <dl>
          <dt>Név</dt>
          <dd>
            <strong>Bernáth Judit</strong>
          </dd>
          <dt>OTP számlaszám</dt>
          <dd>
            <strong>11773377-20100896</strong>
          </dd>
          <dt>Pénznem</dt>
          <dd>
            <strong>Forint</strong>
          </dd>
        </dl>

        <h4>Adatok Euró átutaláshoz</h4>
        <dl>
          <dt>Név</dt>
          <dd>
            <strong>Szilágyi Balázs</strong>
          </dd>
          <dt>IBAN</dt>
          <dd>
            <strong>AT45 1420 0200 1234 3931</strong>
          </dd>
          <dt>BIC</dt>
          <dd>
            <strong>EASYATW1</strong>
          </dd>
          <dt>Pénznem</dt>
          <dd>
            <strong>€ EUR</strong>
          </dd>
        </dl>

        <p>
          Ha úgy érzed eljött az idő és igen! igen! pénzzel szeretnéd támogatni
          a gépírós program fejlesztését, vagy adósnak érzed magad, ne habozz!{' '}
          <strong>Támogatásodat köszönöm!</strong> Igyekszem azt a jó gazda
          gondosságával kezelni.
        </p>

        <h3>Ki kapja ezt a pénzt?</h3>
        <p>
          Személy szerint én,{' '}
          <a
            href="https://www.linkedin.com/in/balázs-szilágyi-112987121"
            target="_blank"
          >
            Szilágyi Balázs
          </a>
          , a program készítője és üzemeltetője, mint nagylelkű ajándékodat
          kapom. 😍
        </p>

        <h3>Mit kapsz ezért a pénzért cserébe?</h3>
        <p>
          Ha pénzt utalsz nekem, kérlek, azt mint ajándékot küldd. Nem szeretném
          kötelesnek érezni magam, hogy számot adjak, mire fordítom.
          Természetesen így vagy úgy a fent megjelölt célokra, vagyis a program
          fenntartására és fejlesztésére fogom fordítani, avagy továbbutalom
          olyanoknak, akik nagymértékben segítettek ebben. A jövőt illetően
          semmire nem tudok és nem is akarok garanciát vállalni. A program már
          évek óta ingyenesen elérhető és bárki szabadon használhatja. (2008
          óta!) Az anyagi hozzájárulásod ennek a szolgáltatásnak a támogatása
          utólag, illetve "motiváció" és költségtérítés a jövőt illetően.
        </p>

        <h3>Köszönetnyilványtás</h3>
        <p>
          Nagyon köszönöm az eddig átutalt összegeket! A 2020 decemberig
          adományozók listája a{' '}
          <a
            href="http://flash.manonet.org/hu/tamogatas/tamogatas.html"
            target="_blank"
          >
            régi oldalon
          </a>{' '}
          megtekinthető. A 2017 - 2020 közötti támogatások ezen időszak domain
          és tárhely költségeinek harmadát fedezték. 2021-től a továbbiakban nem
          szeretném a támogatók listáját feltűntetni, mivel ez is időigényes
          "kézzel végzett" folyamat, e helyett inkább a programozásra
          fókuszálnék.
        </p>

        <h3>Egyéb kérdésed van?</h3>
        <p>
          Kérlek <Link to={ROUTE_PATH_CONTACT}>írd meg!</Link>
        </p>
      </>
    );
  }

  return (
    <Layout className="contribution">
      <SEO
        title={intl.formatMessage({
          id: 'site.contribution',
          defaultMessage: 'Contribution',
        })}
      />

      <section className="contribution__section">
        <div className="container">
          <h2>
            <FormattedMessage
              id="site.contribution"
              defaultMessage="Contribution"
            />
          </h2>
          <div>
            {intl.locale === 'hu' && <HungarianContribution />}
            <p>
              <FormattedMessage
                id="contribution.translate.title"
                defaultMessage="Please help with the translation of the page."
              />
            </p>
            <p>
              <FormattedMessage
                id="contribution.translate.desc"
                defaultMessage="If you understand English (or Hungarian), please help people to use this application in their own native language. Click on the link below for further details:"
              />{' '}
              <ul>
                <li>
                  Details on{' '}
                  <a
                    href="https://gitlab.com/zyxneo/typing/-/blob/dev/CONTRIBUTING.md#translations"
                    target="_blank"
                  >
                    gitlab
                  </a>
                </li>
                <li>
                  Project on{' '}
                  <a
                    href="https://poeditor.com/join/project/eIB3WJcqZ9"
                    target="_blank"
                  >
                    poeditor
                  </a>{' '}
                  translation management system
                </li>
              </ul>
            </p>
          </div>
        </div>
      </section>
    </Layout>
  );
}
