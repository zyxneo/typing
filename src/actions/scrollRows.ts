// SCROLL_ROWS

export type ScrollRowsAction = ReturnType<typeof scrollRows>;

export const SCROLL_ROWS = 'SCROLL_ROWS';

export function scrollRows(rowIndex: number) {
  return { type: SCROLL_ROWS, rowIndex };
}
