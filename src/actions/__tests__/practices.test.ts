import {
  startPractice,
  pausePractice,
  continuePractice,
} from '@actions/practices';

describe('practice actions', () => {
  // not used
  it('should create an action to start practice', () => {
    const expectedAction = {
      type: 'START_PRACTICE',
    };

    expect(startPractice()).toEqual(expectedAction);
  });

  // not used
  it('should create an action to pause practice', () => {
    const expectedAction = {
      type: 'PAUSE_PRACTICE',
    };

    expect(pausePractice()).toEqual(expectedAction);
  });

  // not used
  it('should create an action to continue practice', () => {
    const expectedAction = {
      type: 'CONTINUE_PRACTICE',
    };

    expect(continuePractice()).toEqual(expectedAction);
  });
});
