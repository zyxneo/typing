import {
  keyDown,
  keyUp,
  inputChange,
  flushKeyboard,
  resetState,
} from '@actions/typing';

/*
const keyDownEventForKeyA = {
  altKey: false,
  bubbles: true,
  cancelBubble: false,
  cancelable: true,
  charCode: 0,
  code: 'KeyA',
  composed: true,
  ctrlKey: false,
  currentTarget: null,
  defaultPrevented: false,
  detail: 0,
  eventPhase: 0,
  isComposing: false,
  isTrusted: true,
  key: 'a',
  keyCode: 65,
  layerX: 0,
  layerY: 0,
  location: 0,
  metaKey: false,
  rangeOffset: 0,
  rangeParent: null,
  repeat: false,
  returnValue: true,
  shiftKey: false,
  timeStamp: 31760,
  type: 'keydown',
  which: 65,
  getModifierState: function (key: string) {
    return false;
  },
};

const keyUpEventForKeyA = {
  altKey: false,
  bubbles: true,
  cancelBubble: false,
  cancelable: true,
  charCode: 0,
  code: 'KeyA',
  composed: true,
  ctrlKey: false,
  currentTarget: null,
  defaultPrevented: false,
  detail: 0,
  eventPhase: 0,
  isComposing: false,
  isTrusted: true,
  key: 'a',
  keyCode: 65,
  layerX: 0,
  layerY: 0,
  location: 0,
  metaKey: false,
  rangeOffset: 0,
  rangeParent: null,
  repeat: false,
  returnValue: true,
  shiftKey: false,
  timeStamp: 7574,
  type: 'keyup',
  which: 65,
};
*/

describe('typing actions', () => {
  it('should create an action when a keyboard key is pressed', () => {
    const eventProps = {
      code: 'KeyA',
      key: 'a',
      timeStamp: 31760,
    };
    const expectedAction = {
      type: 'KEY_DOWN',
      eventProps: {
        code: 'KeyA',
        key: 'a',
        timeStamp: 31760,
      },
    };

    expect(keyDown(eventProps)).toEqual(expectedAction);
  });

  it('should create an action when a keyboard key is released', () => {
    const eventProps = {
      code: 'KeyA',
      key: 'a',
      timeStamp: 7574,
    };
    const expectedAction = {
      type: 'KEY_UP',
      eventProps: { ...eventProps },
    };

    expect(keyUp(eventProps)).toEqual(expectedAction);
  });

  it('should create an action when the content of the text input changes', () => {
    const expectedAction = {
      type: 'INPUT_CHANGE',
      userText: 'text typed by the user',
    };

    expect(inputChange('text typed by the user')).toEqual(expectedAction);
  });

  it('should create an action to erase the keyboard memory', () => {
    const expectedAction = {
      type: 'FLUSH_KEYBOARD',
    };

    expect(flushKeyboard()).toEqual(expectedAction);
  });

  it('should create an action to reset the typing state to initial', () => {
    const expectedAction = {
      type: 'RESET_STATE',
    };

    expect(resetState()).toEqual(expectedAction);
  });
});
