import { scrollRows } from '@actions/scrollRows';

describe('scrollRows actions', () => {
  it('should create an action to scroll the practice rows to the given offset', () => {
    const expectedAction = {
      type: 'SCROLL_ROWS',
      rowIndex: 2,
    };

    expect(scrollRows(2)).toEqual(expectedAction);
  });
});
