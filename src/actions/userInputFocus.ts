// USER_INPUT_FOCUS

export type UserInputFocusAction = ReturnType<
  typeof getUserInputFocus | typeof setUserInputFocus
>;

export const GET_USER_INPUT_FOCUS = 'GET_USER_INPUT_FOCUS';

export function getUserInputFocus(focus: boolean) {
  return { type: GET_USER_INPUT_FOCUS, focus };
}

export const SET_USER_INPUT_FOCUS = 'SET_USER_INPUT_FOCUS';

export function setUserInputFocus(focus: boolean) {
  return { type: SET_USER_INPUT_FOCUS, focus };
}
